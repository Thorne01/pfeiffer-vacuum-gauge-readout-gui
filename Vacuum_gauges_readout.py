# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 13:51:57 2022

@author: Jacob
"""

import serial
import time

connect_failure = -1
wait = 0.1 # between trans. and recieve in sec

C = { 
  'ETX': "\x03", # End of Text (Ctrl-C)   Reset the interface
  'CR':  "\x0D", # Carriage Return        Go to the beginning of line
  'LF':  "\x0A", # Line Feed              Advance by one line
  'ENQ': "\x05", # Enquiry                Request for data transmission
  'ACQ': "\x06", # Acknowledge            Positive report signal
  'NAK': "\x15", # Negative Acknowledge   Negative report signal
  'ESC': "\x1b", # Escape
}

LINE_TERMINATION=C['CR']+C['LF'] # CR, LF and CRLF are all possible (p.82)

### Mnemonics as defined on p. 85
M = [
  'BAU', # Baud rate                           Baud rate                                    95
  'CAx', # Calibration factor Sensor x         Calibration factor sensor x (1 ... 6)        92
  'CID', # Measurement point names             Measurement point names                      88
  'DCB', # Display control Bargraph            Bargraph                                     89
  'DCC', # Display control Contrast            Display control contrast                     90
  'DCD', # Display control Digits              Display digits                               88
  'DCS', # Display control Screensave          Display control screensave                   90
  'DGS', # Degas                               Degas                                        93
  'ERR', # Error Status                        Error status                                 97
  'FIL', # Filter time constant                Filter time constant                         92
  'FSR', # Full scale range of linear sensors  Full scale range of linear sensors           93
  'LOC', # Parameter setup lock                Parameter setup lock                         91
  'NAD', # Node (device) address for RS485     Node (device) address for RS485              96
  'OFC', # Offset correction                   Offset correction                            93
  'OFC', # Offset correction                   Offset correction                            93
  'PNR', # Program number                      Program number                               98
  'PRx', # Status, Pressure sensor x (1 ... 6) Status, Pressure sensor x (1 ... 6)          88
  'PUC', # Underrange Ctrl                     Underrange control                           91
  'RSX', # Interface                           Interface                                    94
  'SAV', # Save default                        Save default                                 94
  'SCx', # Sensor control                      Sensor control                               87
  'SEN', # Sensor on/off                       Sensor on/off                                86
  'SPx', # Set Point Control Source for Relay xThreshold value setting, Allocation          90
  'SPS', # Set Point Status A,B,C,D,E,F        Set point status                             91
  'TAI', # Test program A/D Identify           Test A/D converter identification inputs    100
  'TAS', # Test program A/D Sensor             Test A/D converter measurement value inputs 100
  'TDI', # Display test                        Display test                                 98
  'TEE', # EEPROM test                         EEPROM test                                 100
  'TEP', # EPROM test                          EPROM test                                   99
  'TID', # Sensor identification               Sensor identification                       101
  'TKB', # Keyboard test                       Keyboard test                                99
  'TRA', # RAM test                            RAM test                                     99
  'UNI', # Unit of measurement (Display)       Unit of measurement (pressure)               89
  'WDT', # Watchdog and System Error Control   Watchdog and system error control           101
]


### Error codes as defined on p. 97
ERR_CODES = [
  {
        0: 'No error',
        1: 'Watchdog has responded',
        2: 'Task fail error',
        4: 'IDCX idle error',
        8: 'Stack overflow error',
       16: 'EPROM error',
       32: 'RAM error',
       64: 'EEPROM error',
      128: 'Key error',
     4096: 'Syntax error',
     8192: 'Inadmissible parameter',
    16384: 'No hardware',
    32768: 'Fatal error'
  } ,
  {
        0: 'No error',
        1: 'Sensor 1: Measurement error',
        2: 'Sensor 2: Measurement error',
        4: 'Sensor 3: Measurement error',
        8: 'Sensor 4: Measurement error',
       16: 'Sensor 5: Measurement error',
       32: 'Sensor 6: Measurement error',
      512: 'Sensor 1: Identification error',
     1024: 'Sensor 2: Identification error',
     2048: 'Sensor 3: Identification error',
     4096: 'Sensor 4: Identification error',
     8192: 'Sensor 5: Identification error',
    16384: 'Sensor 6: Identification error',
  }
]

### pressure status as defined on p.88
PRESSURE_READING_STATUS = {
  0: 'Measurement data okay',
  1: 'Underrange',
  2: 'Overrange',
  3: 'Sensor error',
  4: 'Sensor off',
  5: 'No sensor',
  6: 'Identification error'
}

def connect(port):
    fd = serial.Serial(port, baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS,timeout=1)
    if fd.isOpen(): #check if the port is open
        return fd
    else:
        return connect_failure
        
def write_message(fd,msg):
    errmsg = None
    fd.write(msg.encode('ascii') + LINE_TERMINATION.encode('ascii')) #always message ends with CR LF
    fd.flush()
    time.sleep(wait) #not sure if these are required...
    serout = fd.readline()
    print("should be ACK:" + str(serout))

    if(serout == C['NAK'].encode('ascii') + LINE_TERMINATION.encode('ascii')):#check that it does not send back NAK, if it does then an error has occurred
        fd.write("ERR".encode('ascii') + LINE_TERMINATION.encode('ascii'))#if NAK is recivied then check error code
        fd.flush()
        time.sleep(wait)
        error = fd.readline()
        print("having sent error:" + str(error))
        fd.write(C['ENQ'].encode('ascii') + LINE_TERMINATION.encode('ascii'))#confirm command
        fd.flush()
        time.sleep(wait)
        error = fd.readline().decode("utf-8")#format in a readable way
        print(str(error))
        errmsg = {'System Error': ERR_CODES[0][int(error.split(',')[0])], 'Gauge Error': ERR_CODES[1][int(error.split(',')[1])]}
        #errmsg returns as (#,#) therefore this line splits the error code into system error and gauge error
    else:
        fd.write(C['ENQ'].encode('ascii') + LINE_TERMINATION.encode('ascii'))#if no error then confirm command is sent
        fd.flush()
        time.sleep(wait)  
    return errmsg #return errmsg, if none then error handling on the upper end will do nothing, only check for filled error message

def read_message(fd):
    read = fd.readline().decode("utf-8")#read then if what is read is NAK will pass error
    print(read)
    if(read == C['NAK'].encode('ascii') + LINE_TERMINATION.encode('ascii')):
        read = "Negative acknowledgement on read!"#no need for error code as read should only give back NAK if something is really wrong in communication
    return read #returns the message or the error message if NAK occurs 
    
def identify(fd):
    error = write_message(fd, "PNR")#gives programme ID
    print(error)
    if error == None:#checks if error is empty or not, if full then error message is passed and that will return in the function for the user
        error = read_message(fd)        
    return error

def set_channel(fd, channel, channel_state):#this function does not work on gauges that are on by default
    string = ",0,0,0,0,0,0"#format required to set channels on or off, 0 is no change, 1 is off, 2 is on
    channel_state = str(channel_state)
    temp = list(string)#takes the numbers for channel_state to make into a string to send
    temp[channel*2 -1] = channel_state#iterates over the "string" and replaces the zeros with 1 or 2's depending on what sensors should be on or off
    writen = "".join(temp)#combines the message with the changes "string"
    error = write_message(fd, "SEN "+writen)#command SEN followed by the sensor status "writen"
    if error == None:
        error = read_message(fd)#error check
    return error

def get_channel_status(fd):
    error = write_message(fd, "TID")#quiries all sensor status, returns string on sensor types and is no sensor is present
    if error == None:
        error = read_message(fd).replace(",",", ")  
    return error

def measure_pressure(fd, channel):
    error = write_message(fd, "PR" + str(channel))#command to get pressure is PR followed by channel number 1-6 for Maxigauge
    if error == None:
        error = read_message(fd)
        error = "Pressure: " + str(float(error.split(',')[1])) + " mbar" + ", " + "Status: " + str(PRESSURE_READING_STATUS[int(error[0])]) 
        #"error" splits the message as it always returns (#, #.##) where the first number is the sensor status, second the pressure
        #the pressure is always read in mbar
        #the pressure reading status is printed out with the pressure
    return error

def main():
    #if (len(sys.argv) != 3):
    #    print("usage: python3 rampHV.py setVoltage setRampSpeed")
    #    sys.exit(0)

    #setVolt = int(sys.argv[1])  # destination voltage in Volts
    #ramp_rate = int(sys.argv[2])  # ramp speed in V/s

    serial_port = "COM5"
    fd = connect(serial_port)
    if(fd == -1):
        return -1
    else:
        print(serial_port + " is connected!")
    
    msg = identify(fd)
    print(msg)
    
    #msg = set_channel(fd, 2, 2) #2 is on, 1 is off, 0 is no change
    #print(msg)
    
    msg = get_channel_status(fd)
    print(msg)
    
    n=0
    message = []
    #while(n != 3):
    
    #    for i in range (1, 7):
    #        msg = measure_pressure(fd, i)
    #        msg = msg.split(' ')[1] + " mbar"
    #        message.append(msg)
            
    #    print(message[2])
    #    time.sleep(1)
    #    n+=1
    #serout = fd.readline()
    #curVolt = get_measured_voltage(ser,1)

    #if (setVolt > 2900):
    #    print("voltage too high")
    #    sys.exit(1)

    #if (ramp_rate > 5):
    #    print("ramp speed too high")
    #    sys.exit(1)

    #if (ramp_rate < 2):
    #    print("ramp speed too low, setting to 2 V/sec")
    #    ramp_rate = 2

    #if (setVolt == curVolt):
    #    print("already at that voltage!")
    #    sys.exit(0)

    #if (setVolt > curVolt):
    #    rampDir = 1

    #if (setVolt < curVolt):
    #    rampDir = -1


    #print("current voltage: " + str(curVolt) + " V")
    #print("set voltage: " + str(setVolt) + " V")
    #print("set speed: " + str(ramp_rate) + " V/s")
    #print("ramp direction: " + str(rampDir) + "   (-1: down, 1: up)")
    #while (1):
    #    response = input('continue? y/n: ')
    #    if (response in ['n', 'N']):
    #        sys.exit(0)
    #    if (response in ['y', 'Y']):
    #        print("ramping...")
    #        break

    #write_set_ramp_rate(ser, 1, ramp_rate)
    #write_set_voltage(ser, 1, setVolt)
    #get_set_voltage(ser, 1)
    #get_set_ramp_rate(ser, 1)
    #write_start_ramp(ser, 1)

    fd.close()
    if(fd.is_open):
        print("Error closing serial")

if __name__== "__main__":
  main()