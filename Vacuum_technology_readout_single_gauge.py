# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 10:31:09 2022

@author: Jacob Thorne
"""
#import sys
#sys.path.append('/Users/Jacob/Documents/Postdoc/Teaching/Read vacuum gauges/exe')
#C:\Users\Jacob\Documents\Postdoc\Teaching
import PySimpleGUI as gui
import threading
import Vacuum_gauges_readout_single_gauge as gauge_functions
import datetime
import time

def continuous_read():
    while True:
        if filename == None:
            gui.popup("Filename not given to record! Please restart")
            break
        message = gauge_functions.measure_pressure(fd) #maybe message needs to be defined?
        message = message.split(' ')[1] + " mbar"
        f = open(filename, "a")
        date = datetime.datetime.now()#unixtime
        #print(str(date.timestamp()) + ",  " + msg[0] + ",  " + msg[1] + ",  " + msg[2] + ",  " + msg[3] + ",  " + msg[4] + ",  " + msg[5])
        string_test = str(date.timestamp()) + ",  " + message + '\n'
        f.write(string_test)
        f.close()#records data about every 1-2 seconds
        time.sleep(1)
        if stop_flag == True:
            break

# Select theme
gui.theme('LightGray1')

filename = gui.popup_get_file('Enter the file to record the data')
#gui.popup('You entered:', filename)
COM_port = gui.popup_get_text('Enter COM port to connect i.e. COM4')

layout = [
    [gui.Text('')],
    [gui.Button('Request programme ID')],
    [gui.Text("", size=(0, 1), key='Programme ID')],
    [gui.Button('Get channel status')],
    [gui.Text("", size=(0, 1), key='Channel status')],
    
    [gui.Button('Measure pressure')],
    [gui.Text("", size=(0, 1), key='Pressure')],
    [gui.Text("", size=(0, 1), key='Sensor status')],
 
    [gui.Button('On'), gui.Button('Off')],
    [gui.Text('')],
    
    [gui.B('Start Recording'), gui.Button('Stop')],
    
    [gui.Button('Close')]
]

window = gui.Window("Vacuum technology readout single gauge", layout)

fd = gauge_functions.connect(COM_port)

#count = []
stop_flag = False

while True:
    
    event, values = window.read()
   
    if event == 'Request programme ID':
        msg = gauge_functions.identify(fd)
        window['Programme ID'].update(msg)
    if event == 'Get channel status':
        msg = gauge_functions.get_channel_status(fd)
        window['Channel status'].update(msg)
    if event == 'Measure pressure':
        msg = gauge_functions.measure_pressure(fd)
        window['Pressure'].update(msg.split(' ')[1] + " mbar")
        window['Sensor status'].update(msg.split(': ')[2])
        
    if  event == 'On':
            gauge_functions.set_channel(fd, 2)
    if  event == 'Off':
            gauge_functions.set_channel(fd, 1)
            
    if event.startswith('Start'):
        stop_flag = False
        threading.Thread(target=continuous_read, daemon=True).start() #args=(count,)
    
    if event == 'Stop':
        stop_flag = True
        
    if event == gui.WIN_CLOSED or event == 'Close': # if user closes window or clicks cancel
        break

window.close()
fd.close()